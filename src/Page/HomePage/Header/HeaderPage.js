import React, { useEffect, useState } from "react";
import { Carousel } from "antd";
import { movieService } from "../../../services/movie.service";
import { RightOutlined, LeftOutlined } from "@ant-design/icons";
import "./HeaderPage.css";
const contentStyle = {
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};
export default function HeaderPage() {
  const [banner, setBanner] = useState([]);
  useEffect(() => {
    movieService
      .getBannerImg()
      .then((res) => {
        setBanner(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div className="w-full">
      <div>
        <Carousel
          arrows
          prevArrow={<LeftOutlined />}
          nextArrow={<RightOutlined />}
        >
          {banner.map((image) => {
            return (
              <div>
                <div style={contentStyle}>
                  <img
                    src={image.hinhAnh}
                    key={image.maBanner}
                    className=" w-full object-cover"
                    style={{ minHeight: 200, height: "600px" }}
                  />
                </div>
              </div>
            );
          })}
        </Carousel>
      </div>
    </div>
  );
}
