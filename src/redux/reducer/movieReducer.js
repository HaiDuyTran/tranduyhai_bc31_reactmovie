import {
  SET_DANG_CHIEU,
  SET_MOVIE_LIST,
  SET_SAP_CHIEU,
} from "../constant/movieConstant";

const initialState = {
  movieList: [],
  movieListDefault: [],
  dangChieu: true,
  sapChieu: true,
};

export const movieReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_MOVIE_LIST:
      state.movieList = payload;
      state.movieListDefault = state.movieList;
      return { ...state };
    case SET_DANG_CHIEU:
      state.dangChieu = !state.dangChieu;
      state.movieList = state.movieListDefault.filter(
        (film) => film.dangChieu === state.dangChieu
      );
      return { ...state };
    case SET_SAP_CHIEU:
      state.sapChieu = !state.sapChieu;
      state.movieList = state.movieListDefault.filter(
        (film) => film.sapChieu === state.sapChieu
      );
      return { ...state };
    default:
      return state;
  }
};
// rxreducer
