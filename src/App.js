import "./App.css";
import LoginPage from "./Page/LoginPage/LoginPage";
import HomePage from "./Page/HomePage/HomePage";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Layout from "./HOC/Layout";
import DetailPage from "./Page/DetailPage/DetailPage";
import SpinnerComponent from "./component/SpinnerComponent/SpinnerComponent";
import RegisterPage from "./Page/RegisterPage/RegisterPage";

function App() {
  return (
    <div className="">
      <SpinnerComponent />
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Layout
                // Component={HomePage}
                Component={HomePage}
              />
            }
          />
          <Route
            path="/detail/:id"
            element={<Layout Component={DetailPage} />}
          />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<RegisterPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
