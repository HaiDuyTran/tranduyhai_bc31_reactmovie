import { BAT_LOADING, TAT_LOADING } from "../constant/spinnerConstant";

export const batLoadingAction = () => {
  return {
    type: BAT_LOADING,
  };
};

export const tatLoadingAction = () => {
  return {
    type: TAT_LOADING,
  };
};
