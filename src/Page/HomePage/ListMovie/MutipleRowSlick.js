import React, { Component } from "react";
import { useEffect } from "react";
import { render } from "react-dom";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import Slider from "react-slick";
import { getMovieListActionService } from "../../../redux/actions/movieAction";
import { styleSlick } from "react-slick";
import {
  SET_DANG_CHIEU,
  SET_SAP_CHIEU,
} from "../../../redux/constant/movieConstant";
import "./MutipleRowSlick.css";

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block" }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block" }}
      onClick={onClick}
    />
  );
}
export default function ListMovie() {
  let dispatch = useDispatch();
  let { movieList } = useSelector((state) => {
    return state.movieReducer;
  });
  const { dangChieu, sapChieu } = useSelector((state) => state.movieReducer);
  useEffect(() => {
    // movieService
    //   .getMovieList()
    //   .then((res) => {
    //     console.log(res.data);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
    dispatch(getMovieListActionService());
  }, []);
  console.log("movieList", movieList);
  const settings = {
    className: "center variable-width",
    centerMode: true,
    infinite: true,
    centerPadding: "0px",
    variableWidth: true,
    slidesToShow: 4,
    speed: 500,
    rows: 2,
    slidesPerRow: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };
  let renderMovieList = () => {
    return movieList
      .filter((item) => {
        return item.hinhAnh !== null;
      })
      .map((item, key) => {
        return (
          <div className="swiper-slide">
            <div className="slide-content">
              {/* <NavLink to={`detail/${item.maPhim}`}>
                <div className="slide-image relative">
                  <img src={item.hinhAnh}></img>
                  <div className="text-black font-medium absolute -bottom-1 left-0 text-4xl font-bold">
                    {key + 1}
                  </div>
                </div>
                <div className="slide-description h-10">
                  <h2 className="text-black text-xl font-medium">
                    {item.biDanh}
                  </h2>
                </div>
              </NavLink> */}
              <div className="flip-card mt-2">
                <div className="flip-card-inner">
                  <div className="flip-card-front">
                    <img
                      src={item.hinhAnh}
                      alt="Avatar"
                      style={{
                        width: 300,
                        height: 300,
                        backgroundSize: "100%",
                      }}
                    />
                  </div>
                  <div
                    className="flip-card-back text-center flex justify-center items-center"
                    style={{
                      position: "relative",
                      backgroundColor: "rgba(0,0,0,.9)",
                    }}
                  >
                    <div style={{ position: "absolute", top: 0, left: 0 }}>
                      <img
                        src={item.hinhAnh}
                        alt="Avatar"
                        style={{
                          width: 300,
                          height: 300,
                          backgroundSize: "100%",
                        }}
                      />
                    </div>
                    <div
                      className="w-full h-full flex justify-center items-center"
                      style={{
                        position: "absolute",
                        backgroundColor: "rgba(0,0,0,.5)",
                      }}
                    >
                      <div>
                        <div>
                          <img
                            src="	https://movie-booking-project.vercel.app/img/carousel/play-video.png"
                            style={{
                              position: "absolute",
                              top: "18%",
                              left: "37%",
                            }}
                            alt="play"
                          />
                        </div>
                        <div className="text-2xl mt-2 font-bold">
                          {item.biDanh}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="mt-2">
                <button
                  onClick={() => {
                    window.location.href = `/detail/${item.maPhim}`;
                  }}
                  className="bg-blue-400 rounded text-center cursor-pointer py-2 my-2 text-success-50 font-bold w-11/12 mr-2"
                >
                  Đặt vé
                </button>
              </div>
            </div>
          </div>
        );
      });
  };

  let activeClassDC = dangChieu === true ? "active_film" : "non_active_film";
  let activeClassSC = sapChieu === true ? "active_film" : "non_active_film";

  return (
    <div className="px-20 mt-10" id="movieList">
      <button
        type="button"
        className={`${activeClassDC} px-8 py-3 text-2xl font-semibold border rounded mr-10`}
        onClick={() => {
          const action = { type: SET_DANG_CHIEU };
          dispatch(action);
        }}
      >
        Đang chiếu
      </button>
      <button
        type="button"
        className={`${activeClassSC} px-8 py-3 text-2xl font-semibold border rounded mb-3`}
        onClick={() => {
          const action = { type: SET_SAP_CHIEU };
          dispatch(action);
        }}
      >
        Sắp chiếu
      </button>
      <Slider {...settings}>{renderMovieList()}</Slider>
    </div>
  );
}
