export let loginAction = (dataLogin) => {
  return {
    type: "LOGIN",
    payload: dataLogin,
  };
};

export let registerAction = (dataRegister) => {
  return {
    type: "REGISTER",
    payload: dataRegister,
  };
};
