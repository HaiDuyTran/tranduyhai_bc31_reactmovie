import { https } from "./configURL";

export let movieService = {
  getMovieList: () => {
    return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05");
  },
  // getMovieDetail: (id) => {
  //   return https.get(
  //     `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`
  //   );
  // },
  getMovieByTheater: () => {
    return https.get(
      `https://movienew.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP05`
    );
  },
  getBannerImg: () => {
    return https.get(
      `http://movieapi.cyberlearn.vn/api/QuanLyPhim/LayDanhSachBanner`
    );
  },
  layThongTinLichChieuPhim : (id) => { 
    return https.get(
      `https://movienew.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`
    )
   }
};
