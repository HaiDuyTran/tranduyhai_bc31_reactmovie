import { movieService } from "../../services/movie.service";
import { SET_MOVIE_LIST } from "../constant/movieConstant";

export const getMovieListActionService = () => {
  return (dispatch) => {
    movieService
      .getMovieList()
      .then((res) => {
        dispatch({
          type: SET_MOVIE_LIST,
          payload: res.data.content,
        });
      })
      .catch((err) => {
        
      });
  };
};
