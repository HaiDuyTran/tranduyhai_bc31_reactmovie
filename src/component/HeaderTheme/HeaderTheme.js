import React from "react";
import UserNav from "./UserNav";
import "./HeaderTheme.css";
import { NavLink } from "react-router-dom";

export default function HeaderTheme() {
  return (
    <div className="header container">
      <div className="header_nav px-10 py-1 mx-auto">
        <div style={{ color: "white" }} className="logo text-2xl font-medium">
          <img
            width="200"
            height="50"
            src="https://cybersoft.edu.vn/wp-content/uploads/2021/03/logo-cyber-nav.svg"
            alt=""
          ></img>
        </div>
        <div>
          <ul className="flex text-white">
            <li className="flex">
              <a
                rel="noopener noreferrer"
                href="#movieList"
                className="flex items-center text-xl px-4  dark:text-violet-400 dark:border-violet-400"
              >
                Lịch chiếu
              </a>
            </li>
            <li className="flex">
              <a
                rel="noopener noreferrer"
                href="#cumRap"
                className="flex items-center text-xl px-4 "
              >
                Cụm rạp
              </a>
            </li>
            <li className="flex">
              <a
                rel="noopener noreferrer"
                href="#"
                className="flex items-center text-xl px-4 "
              >
                Tin tức
              </a>
            </li>
            <li className="flex">
              <a
                rel="noopener noreferrer"
                href="#"
                className="flex items-center text-xl px-4 "
              >
                Ứng dụng
              </a>
            </li>
          </ul>
        </div>
        <div>
          <UserNav />
        </div>
      </div>
    </div>
  );
}
