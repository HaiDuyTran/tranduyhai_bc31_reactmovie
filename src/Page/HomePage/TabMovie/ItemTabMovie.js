import React from "react";
import moment from "moment";
export default function ItemTabMovie({ phim }) {
  return (
    <div className="flex gap-2 hover:shadow-2xl hover:shadow-slate-500">
      <img src={phim.hinhAnh} className="w-40 h-48 object-cover" alt="" />
      <div>
        <p className="text-2xl font-medium text-gray-800 mb-5">
          {phim.tenPhim}
        </p>
        <div className="  gap-2 grid grid-cols-4">
          {phim.lstLichChieuTheoPhim.slice(0, 8).map((lichChieu) => {
            return (
              <div
                key={lichChieu.maLichChieu}
                className="bg-red-500 text-white rounded p-2"
              >
                {moment(lichChieu.ngayChieuGioChieu).format("DD-MM-YYYY")}
                <span className="text-yellow-200 font-bold text-base ml-5">
                  {moment(lichChieu.ngayChieuGioChieu).format("hh:mm")}
                </span>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
