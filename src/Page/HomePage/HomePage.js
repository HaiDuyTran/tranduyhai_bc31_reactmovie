import React from "react";
import HeaderPage from "./Header/HeaderPage";
import ListMovie from "./ListMovie/MutipleRowSlick";
import TabMovie from "./TabMovie/TabMovie";

export default function HomePage() {
  return (
    <div>
      <HeaderPage />
      <ListMovie />
      {/* <ListMovie /> */}
      <TabMovie />
    </div>
  );
}
